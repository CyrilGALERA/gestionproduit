<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class IndexController extends AbstractController
{
    #[Route('/', name: 'index_index',methods:['GET'])]
    public function index()
    {
        //$contex= array('titre' => 'Bienvenue Gestion de Produit');
        //$contex= array('titre' => 'Bienvenue Gestion de Produit');

        // $contex= array('titre' => 'Liste des produits' , 
        //     'products'=> $productRepository->findAll(), 
        //     'showEdit'=> false
        // );

        // $contex= array('titre' => 'Veuillez vous connecter' , 
        //     'products'=> $productRepository->findAll(), 
        //     'showEdit'=> false
        // );

        return $this->render('index.html.twig');
    }
}